## My best wear

Votre client a décidé de se lancer dans l'aventure de l'entrepreuneuriat, 
et souhaite lancer un service de fabrication de t-shirts personnalisés 
sur internet.

L'idée est que les clients pourront uploader une photo de leur choix, 
choisir le modèle, la taille et la couleur du t-shirt, la position de 
l'impression (devant, derrière, sur la manche), ainsi que la dimension 
du sticker imprimé. Finalement, le client sera invité à procéder à une 
commande en ligne : quelle quantité commandée, quelle adresse de livraison, 
rappel des tarifs, ainsi qu'une case à cocher pour valider l'engagement 
du client à payer la commande. Le paiement en ligne n'est pas requis.

Par ailleurs, le site doit proposer l'ensemble des informations qu'on 
peut attendre d'un site de vente en ligne : conditions générales 
d'utilisation, conditions générale de vente, page de contact, RGPD, 
coordonnées de l'entreprise incluant une ligne téléphonique. En page 
d'accueil, un carousel affichant des exemples de produits finis doit 
apparaître.

L'interface doit évidemment respecter les bonnes pratiques de la 
profession : être responsive et adaptée à tous les publiques, y compris 
les personnes en situation de handicap dans la mesure du possible.

Ce projet pédagogique est à réaliser par une équipe de trois développeurs 
sur une période de une semaine.


# Bonus

Si pendant le process de commande une représentation du t-shirt customisé 
pouvait apparaître, ce serait top !