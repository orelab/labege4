## My best wear

Votre client, un patissier un peu geek, a inventé une machine qui 
permet d'imprimer des photos sur ses gateaux avec du colorant 
alimentaire ! Pour honorer ses commandes, il a décidé de mettre en 
place un site internet qui doit permettre à ses clients de passer 
commande, en spécifiant les options possibles, qui sont :

- le modèle du gateau
- la taille de l'impression, en pourcentage de la largeur du gateau
- la taille, en nombre de parts (de 8 à 20, en nombres pairs)
- la date de retrait souhaitée

Par ailleurs, les gateaux peuvent être filtrés par restriction 
alimentaire : gluten, lactose, etc...

Au terme de cette étape de configuration, le client sera invité à 
cocher une case pour valider son engagement à venir chercher et 
payer sur place son gateau.

Par ailleurs, le site doit proposer l'ensemble des informations qu'on 
peut attendre d'un site de vente en ligne : conditions générales 
d'utilisation, conditions générale de vente, page de contact, RGPD, 
coordonnées de l'entreprise incluant une ligne téléphonique. En page 
d'accueil, un carousel affichant des exemples de gateaux doit 
apparaître.

L'interface doit évidemment respecter les bonnes pratiques de la 
profession : être responsive et adaptée à tous les publiques, y compris 
les personnes en situation de handicap dans la mesure du possible.

Ce projet pédagogique est à réaliser par une équipe de trois développeurs 
sur une période de une semaine.


# Bonus

Si pendant le process de commande une représentation du gateau
pouvait apparaître, ce serait top !