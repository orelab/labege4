
jQuery( document ).ready(function() {

    jQuery('#formulaire_vetements img,#formulaire>div>div>div').on('click', doClickImage);
    
    jQuery('#formulaire>div>div>div').on('click', doChangeColor);
});


function doClickImage(){
    jQuery('#formulaire_vetements img').css('border', 'none');
    jQuery(this).css('border', '3px grey solid');
    jQuery(this).next().click();
}

function doChangeColor(){
    let color = this.className;
    jQuery('#formulaire_vetements img').css('background-color', color);
}