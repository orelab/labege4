<?php
/**
 * @package Formulaire_Orel
 * @version 1.0.0
 *
 * Plugin Name: Formulaire Orel
 * Plugin URI: http://funworks.fr
 * Description: Here is a simple form
 * Author: Orel
 * Version: 1.0.0
 * Author URI: http://funworks.fr
*/





/*
 *
 * ADDING CSS & JS
 * 
 */

function formulaire_add_scripts() {
    wp_enqueue_script('jquery' );

    wp_enqueue_script( 
        'formulaire', 
        plugin_dir_url('') . "formulaire/html/app.js", 
        array('jquery'), 
        rand(), 
        true
    );

    wp_enqueue_style(
        'formulaire', 
        plugin_dir_url('') . "formulaire/html/style.css", 
        array(), 
        rand()
    );
}

add_action('wp_enqueue_scripts', 'formulaire_add_scripts');



/*
 *
 *  ADDING SHORTCODE
 *  
 */

function formulaire_custom_shortcode( $atts ){
    $dir = plugin_dir_path( __FILE__ );
    $file = $dir . "html/form.html";
    $html = file_get_contents($file);
    return $html;
}

add_shortcode( 'formulaire', 'formulaire_custom_shortcode' );