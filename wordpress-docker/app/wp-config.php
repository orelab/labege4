<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wordpress' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '**xU_gtZ}&qnb$S}a?#Y6oa)Ru.M_@K3/%uOAg cowH0DGDm)VsD0$6,7T$+`5Tr' );
define( 'SECURE_AUTH_KEY',  '_1wHKLV]o.%eDqI5AQSfW,KW3XB_1Fuc{{4g]e#z^[qj$!+V0H~m#vO7#9EJe[~>' );
define( 'LOGGED_IN_KEY',    '-h[McU+izC<.6myTb.90Yf4; Dj HFpDy hH|6Ccg3sRtL} M,vW@{5(vOfS_A.5' );
define( 'NONCE_KEY',        'cnu|_!4s&WN`9^c*i YNpTy2~@:PK)C!FboZlC~b@@ykp0fX9:JQTL,3qFz#!%1t' );
define( 'AUTH_SALT',        'zQD1D+n%/#LJzGG=G;QP0%D|>>Kz;~`SW&}d|UQ>].j_|t|UvZD]:aM/6Rd0*#*[' );
define( 'SECURE_AUTH_SALT', ';gol8N7E|NhK`A|w{-y@>B]aVUjly/V/Ni!t<rv2Gd,~z<>-cBr`-H<O@DfRY8yq' );
define( 'LOGGED_IN_SALT',   ';k.f@ttaMcbqAtI_J4P{j$`n;*t^1}5eOI]|D[QN1Ocxjm/vOly`(3jKKk^Z~1Q,' );
define( 'NONCE_SALT',       '~%)c waG@Tf/7]E,=VA4,i?cUYf/5Y:,_7= V^s)nm&ExCy_[+]5B(RAhOXrLr(j' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
