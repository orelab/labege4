<?php


function get_project($id){
    global $cnx;
    $sth = $cnx->prepare("SELECT * FROM projects WHERE ID = :id;");
    $sth->execute(["id"=>$id]);
    return $sth->fetchAll();
}



function get_all_projects(){
    global $cnx;
    $sth = $cnx->prepare("SELECT * FROM projects;");
    $sth->execute();
    return $sth->fetchAll();
}



function add_project($name, $detail){
    global $cnx;
    $sth = $cnx->prepare("INSERT INTO projects (name, detail) VALUES (:name, :detail);");
    $sth->execute([":name"=>$name, ":detail"=>$detail]);
    return $cnx->lastInsertId();
}



function delete_project($id){
    global $cnx;
    $sth = $cnx->prepare("DELETE FROM projects WHERE id = :idproject;");
    return $sth->execute(["idproject"=>$id]);
}



function update_project($id, $name, $detail){
    global $cnx;
    $sth = $cnx->prepare("UPDATE projects SET name = :name, detail = :detail WHERE id = :id;");
    return $sth->execute(["id"=>$id, "name"=>$name, "detail"=>$detail]);
}