# Intro PHP, mise en pratique !


## Ajouter des propriétés

Ajoutez les propriétés suivantes à votre classe Person :
***surname, birthday, age, mail, phone, road, postcode*** et ***city***.

## les accesseurs et mutateurs

Toutes les propriétés doivent être privées, et vous devrez 
utiliser les méthodes magiques **__get()** et **__set()** pour y accéder
en lecture et en écriture.

Faites en sorte que lorsque l'on change la date de naissance 
d'une personne, sa propriété age doit être automatiquement 
mis à jour.

## Ajouter un constructeur

Ajouter une méthode constructeur, qui prend une variable en paramètre.
Cette variable est un tableau associatif contenant toutes les informations
de la personne. Dans ce constructeur, vous devez mettre à jour toutes les
propriétés, de manière à ce que votre instance soit prête à être utilisée 
dès l'instanciation (c'est-à-dire dans l'instruction où l'on utilise le 
mot-clé **new**).

## Ajouter une méthode d'affichage

Écrire une méthode **show()** qui retourne (avec **return**, pas **echo** !)
le HTML nécessaire à l'affichage d'une carte de visite simple.

## Intégrez dans une page web

- Installez FakerPHP : ```composer require fakerphp/faker```
- Incluez le script ***phonebook.php***
- Faites en sorte que votre carnet d'adresse s'affiche en HTML, grâce à
votre classe, et dans l'ordre alphabétique des noms de famille :)

## Utilisez vos méthodes

Utilisez vos méthodes précédemment écrites **phoneNumberIsValid()** 
et **contains_arobase_and_dot()** pour valider les
valeurs des emails et numéros de téléphone.

## Bonus

Intégrez la possibilité d'enregistrer les liens de parenté entre les 
personnes (Google : ***oop aggregation, oop composition***)