## Twitter with MaterializeCSS

# useful links

- https://materializecss.com/
- https://fonts.google.com/icons
- https://cordova.apache.org/docs/en/10.x/guide/platforms/android/


# Run it on Android

- install Android Studio
- install Gradle
- configure JAVA_HOME !

Then :

- sudo npm -install -g cordova
- cd android
- npm install
- cordova platform add android
- cordova platform add browser
- cordova run android


# Bonus

You have a webserver for dev bundled with Cordova :

- cordova run browser