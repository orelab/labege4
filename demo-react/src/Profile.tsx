import React, { ChangeEvent, useState } from "react";
import { GlobalContext } from './App';

type propType = {
    name: string,
    text: string
}


export default function Profile(props: propType) {

    const ctx = React.useContext(GlobalContext);

    const [myName, setName] = useState(props.name);

    const doChange = (ev: ChangeEvent<HTMLInputElement>) => {
        setName(ev.currentTarget.value);
    }

    return (
        <div className="col-md-6">
            <h2>Profile</h2>


            <div className="form-group">
                <label htmlFor="name">
                    {ctx.language === 'en' && 'name'}
                    {ctx.language === 'fr' && 'surnom'}
                    {ctx.language === 'es' && 'nombre'}
                    {ctx.language === 'ar' && 'اسم'}
                </label>
                <input className="form-control" type="text" name="name" value={myName} onChange={doChange} />
            </div>
            <p>{props.text}</p>
            <p className="debug">debug : {ctx.language}</p>
        </div>
    )
}