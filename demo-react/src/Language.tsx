import React from 'react';
import { GlobalContext } from './App';


function Language() {

    const ctx = React.useContext(GlobalContext);

    return (
        <div className="col-md-6">
            <h2>Language</h2>

            <div className="form-group">
                <label htmlFor="language">
                    {ctx.language === 'en' && 'language'}
                    {ctx.language === 'fr' && 'langue'}
                    {ctx.language === 'es' && 'lengua'}
                    {ctx.language === 'ar' && 'لسان'}
                </label>
                <select className="form-control" name="language" onChange={ctx.setLanguage} value={ctx.language}>
                    <option value="en">english</option>
                    <option value="fr">français</option>
                    <option value="es">espanol</option>
                    <option value="ar">العربي</option>
                </select>
            </div>

            <p className="debug">debug : {ctx.language}</p>
        </div>
    );
}

export default Language;