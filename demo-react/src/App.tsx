import React from 'react';
import Language from './Language';
import Profile from './Profile';
import './App.css';


type ctxType = {
  language: string,
  setLanguage: (ev: React.ChangeEvent<HTMLSelectElement>) => void
}

const GlobalContext = React.createContext<ctxType>({
  language: 'en',
  setLanguage: (ev: React.ChangeEvent<HTMLSelectElement>): any => { }
});



function App() {

  const [GlobalState, setGlobalState] = React.useState<ctxType>({
    'language': 'en',
    'setLanguage': (ev: React.ChangeEvent<HTMLSelectElement>): any => {
      setGlobalState({ ...GlobalState, language: ev.currentTarget.value });
    }
  });


  return (
    <GlobalContext.Provider value={GlobalState}>
      <h2>App</h2>

      <input className="form-control"
        onChange={(e) => setGlobalState({ ...GlobalState, language: e.currentTarget.value })}
        value={GlobalState.language} />

      <main>
        <Language />

        <Profile name="Orel" text="React rocks, baby !" />

      </main>

    </GlobalContext.Provider>
  );
}

export default App;
export { GlobalContext };


