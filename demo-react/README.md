# Un projet démo pour appréhender les States et Context dans React

Ce projet définit trois composants : 
- App, qui contient les deux autres composants
- Language, qui permet de sélectionner la langue
- Profile, qui affiche une fiche utilisateur

L'idée est de bénéficier d'un lieu de stockage global partagé, que l'ensemble 
des composants peuvent consulter. Quand un changement a lieu dans le contexte, 
les composants se mettent à jour automatiquement, comme lorssqu'on fait des modifs
dans un simple State local !

Pour arriver à ça, le composant racine partage un State local aux autres composants : 
GlobalState. Ce State contient deux valeurs : la données, ainsi qu'une fonction de mise
à jour de cette donnée

## Sommaire de la veille

1. Components, Props et State
2. Partager un State via un Context
3. Modifier une donnée du Context, depuis un component lambda

## Quelques liens qui m'ont aidé, plus ou moins par ordre de difficulté :

- https://fr.reactjs.org/docs/hooks-state.html
- https://fr.reactjs.org/docs/context.html
- https://fr.reactjs.org/docs/hooks-reference.html#usecontext
- https://fr.reactjs.org/docs/context.html#updating-context-from-a-nested-component
- https://fr.reactjs.org/docs/composition-vs-inheritance.html






# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
