<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body id="home">

    <div class="container">
        
    
        <a href="/save/white" class="btn white">M. White</a>
        <a href="/save/blonde" class="btn blonde">M. Blonde</a>
        <a href="/save/orange" class="btn orange">M. Orange</a>
        <a href="/save/pink" class="btn pink">M. Pink</a>
        <a href="/save/blue" class="btn blue">M. Blue </a>
        <a href="/save/brown" class="btn brown">M. Brown</a>

        
        <table class="table">
            <tr>
                <th>id</th>
                <th>date</th>
                <th>color</th>
                <th>&nbsp;</th>
            </tr>

            @foreach($logs as $log)

            <tr class="{{ $log->color }}">
                <form action="/update">
                    <td>
                        {{ $log->id }}
                        <input type="hidden" name="id" value="{{ $log->id }}" readonly="readonly" />
                    </td>
                    <td>
                        {{ $log->created_at }}
                    </td>
                    <td>
                        <select name="color">
                            <option {{ $log->color=="white"?"selected":"" }} value="white">M. white</option>
                            <option {{ $log->color=="blonde"?"selected":"" }} value="blonde">M. Blonde</option>
                            <option {{ $log->color=="orange"?"selected":"" }} value="orange">M. Orange</option>
                            <option {{ $log->color=="pink"?"selected":"" }} value="pink">M. Pink</option>
                            <option {{ $log->color=="blue"?"selected":"" }} value="blue">M. Blue</option>
                            <option {{ $log->color=="brown"?"selected":"" }} value="brown">M. Brown</option>
                        </select>
                    </td>
                    <td>
                        <button class="btn btn-success">update</button>
                        <a href="/delete/{{ $log->id }}" class="btn btn-danger" >delete</a>
                    </td>
                </form>
            </tr>

            @endforeach
        </table>

    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>