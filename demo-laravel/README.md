# Reservoir Dogs


## Brief

L'objectif du projet est de fournir une page web qui propose 6 boutons : 
M. White, M. Blonde, M. Orange, M. Pink, M. Blue et M. Brown

Le clic sur l'un de ces boutons provoque deux actions :
- l'enregistrement du clic dans une table "log" (id, date, color)
- l'affichage d'une page de remerciement, contenant entre autres un lien de retour à l'accueil

### Bonus

- N'utilisez que des contrôleurs dans le routeur (pas de fonction)
- Affichez le nom de la couleur enregistrée dans la page de remerciement
- Utilisez une [route paramétrée](https://laravel.com/docs/9.x/routing#required-parameters) pour la page de sauvegarde (la couleur doit être une variable dans l'url)
- Utilisez le [stack Front fourni par Laravel](https://laravel.com/docs/5.8/frontend#writing-css)

### Seconde étape

- Affichez le contenu de la table Logs sur la page d'accueil
- Pour chaque ligne, ajoutez un bouton qui permet de supprimer cette ligne de la table (et recharger la page)


## Lancer le projet

- composer install
- ./vendor/bin/sail up

## PhpMyAdmin

C'est moche, mais pour vous aider, vous avez une copie de PhpMyAdmin
accessible dans le dossier public :
- http://localhost/pma/index.php
- user : root (ou sail)
- mot de passe : password

## Fichiers utiles

- /routes/web.php
- /app/Http/Controllers/ColorController.php
- /app/Models/Log.php (dans un premier temps, on ne va rien y faire)
- /resources/views/home.blade.php
- /resources/views/save.blade.php
- /database/migrations/2022_02_24_121404_create_logs_table.php

## Créer un contrôleur

- ./vendor/bin/sail artisan make:controller ColorController --resource
- notez qu'avec sail, on remplace "php" par "./vendor/bin/sail"


## Créer une table et son modèle

- Dans Laravel, un Model est une classe qui permet de manipuler la table MySQL 
avec des listes et objets PHP, plutôt qu'avec du SQL.
- https://laravel.com/docs/master/migrations
- ./vendor/bin/sail artisan make:migration create_logs_table
- customisez le fichier généré dans /database/migrations, puis lancez :
- ./vendor/bin/sail artisan migrate
- ./vendor/bin/sail artisan make:model Log
- notez que, par convention, la table MySQL doit s'appeler logs (avec un 's')

## CSS

J'ai utilisé le stack proposé par Laravel :
- https://laravel.com/docs/5.8/frontend#writing-css
- npm install
- npm run watch
- /resources/css/app.css