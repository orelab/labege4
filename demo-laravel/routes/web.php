<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ColorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ColorController::class, 'index'] );
Route::get('/save/{color}', [ColorController::class, 'save']);
Route::get('/delete/{id}', [ColorController::class, 'delete']);
Route::get('/update', [ColorController::class, 'update']);