<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Log;

class ColorController extends Controller
{

    public function index()
    {
        $logs = Log::all();
        return view('home', ['logs' => $logs]);
    }

    public function save($color)
    {
        $log = new Log;
        $log->color = $color;
        $log->save();

        // return view('save', ['color' => $color]);
        return redirect('/');
    }

    public function delete($id){
        $log = Log::find($id);
        $log->delete();

        return redirect('/');
    }

    public function update(Request $req){
        $log = Log::find($req->input('id'));
        $log->color = $req->input('color'); 
        $log->save();

        return redirect('/');
    }
}
