/*

    Configuration

*/

const types = ["people", "planets", "species", "starships", "vehicles"];

const duration = 30; // duration between card fetches, in minutes (120)



/*

    Stopwatch

*/

function countdown() {
    const lastcall = localStorage.getItem("lastcall");

    if (!lastcall) return;

    return parseInt(lastcall)       //   last call...
        + (1000 * 60 * duration)    // + 2 hours
        - (new Date()).getTime()    // - now
}

function stopwatch() {
    const sec = countdown();

    if (sec > 0) {
        const date = new Date(sec);
        const seconds = date.getSeconds();
        const minutes = date.getMinutes();
        const hours = date.getUTCHours();
        document.querySelector("#countdown").innerHTML = `${hours}:${minutes}:${seconds}`;
    }
    else {
        document.querySelector("#countdown").innerHTML = `<p>OK !</p>`;
    }
}
setInterval(stopwatch, 1000);




/*

    Get a random STARWARS PEOPLE

*/
async function doGetCard() {

    if (countdown() > 0) {
        alert("Il n'est pas encore temps !")
        return;
    }

    const type = types[Math.floor(Math.random() * types.length)];

    const maxi = await fetch(`https://swapi.dev/api/${type}/`)
        .then(data => data.json())
        .then(data => data.count)

    const random = Math.floor(Math.random() * maxi);

    let card = await fetch(`https://swapi.dev/api/${type}/` + random)
        .then(data => data.json())

    if( "detail" in card && card["detail"]==="Not found" ){
        doGetCard();
        return;
    }

    card["type"] = type;        // Adding "type" in data to allow custom class based on type

    localStorage.setItem("lastcall", (new Date()).getTime());   // update time
    appendCard(card);                                           // save card
    redrawCards();                                              // refresh interface
}
document.querySelector("#getcard").addEventListener("click", doGetCard);





/*

    localstorage Database

*/

function getCards(){
    let cardlist = localStorage.getItem("cards");
    
    if( cardlist === null){ // first recording => empty cardlist
        cardlist = "[]";
    }

    return JSON.parse(cardlist);
}

function redrawCards() {
    const html = document.querySelector("#cardlist");
    
    html.innerHTML = "";

    getCards().forEach(c => {
        let content = "";
        for (const [key, value] of Object.entries(c)) {
            content += `<span>${key}</span> ${value}`;
        }
        html.innerHTML += `<div class="card ${c.type}">${content}</div>`;
      })
}

function appendCard(card) {

    cardlist = getCards();
    cardlist.push(card);
    
    localStorage.setItem("cards", JSON.stringify(cardlist));
}

redrawCards();


