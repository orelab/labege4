# Simplon Labège 4 / 2021~2022

- [intro PHP](./intro-php)
- [reproduction Twitter avec Materialize](./materialize)
- [projet PHP/MySQL/BDD](./project-sample)
- [projets pédagos - session 1](./projets-pedagos-1)
- [exemple Wordpress + plugin + Docker](./wordpress-docker)
- [SWAPI game](./swapi)
- [Reservoir Dogs - démo Laravel](./demo-laravel)

